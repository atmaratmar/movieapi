#Assignment 3: Create a Web API and database with Spring
 for Noroff and ExperisAcademy_Denmark

This project follows the outline of the specification provided by Noroff, as outlined below.

Use Hibernate to create a database with the following minimum requirements:

a) Create models and Repositories for Character, Movie and Franchise entities and to cater for these specifications:

    -Business rules
      Characters and Movies: One movie may contain many characters, and a character can play in many movies.
      Movies and Franchises: One movie belongs to one franchise, but a franchise can contain many movies.
    -Data requirements
      The Character entity must contain:
        - An autoincrmented id
        - Full name
        - Alias (if apllicable)
        - Gender
        - Picture (URL)
      The Movie entity must contain:
        - Autoincremented id
        - Movie Title
        - Genre
        - Release year
        - Director
        - Picture (URL)
        - Trailer (URL)
      The Franchise entity must contain:
        -Autoincremented id
        -Name
        -Description

b) Create a Web API in Spring Web with these requirements:

      - Create controllers according to these specifications:
          - CRUD operations to: - Get all Movies in a Franchise
                                - Get all Characters in a Movie
                                - Get all Characcters in a Franchise
                                - Update Characters in a Movie
                                - Update Characters in a Franchise      
                                
          - Swagger/Open API documentation

#This assignment was done using Java in IntelliJ,
Swagger can be acessed via this link when the program is running through port8080:
http://localhost:8080/swagger-ui.html
#Example Data can be found in resources/static/data.sql
# port number and database connection can change in resources/application.properties
# Postman was also used to for endpoint testing and the Json file is included within this repository.
##EndPoints:
### Get All Characters
GET https://localhost:8080/api/v1/character

### Get Character By Id
GET http://localhost:8080/api/v1/character/{{id}}

### Add Character
POST http://localhost:8080/api/v1/character

### Update Character
PUT http://localhost:8080/api/v1/character

### Get All Franchise
GET http://localhost:8080/api/v1/franchise

### Get Franchise By Id
GET http://localhost:8080/api/v1/franchise/{{id}}

### Add New Franchise
POST http://localhost:8080/api/v1/franchise

### Update Franchise
PUT http://localhost:8080/api/v1/franchise

### Delete Franchise
DELETE http://localhost:8080/api/v1/franchise/{{id}}

### Get Movie by Franchise Id
GET http://localhost:8080/api/v1/franchise/{{id}}/movies

### Get Franchise,s Character
GET http://localhost:8080/api/v1/franchise/{{id}}/characters

### Get All Movies
GET http://localhost:8080/api/v1/movie

### Get Movies By Id
GET http://localhost:8080/api/v1/movie/{{id}}

### Add New Move
POST http://localhost:8080/api/v1/movie

### Update Movie
PUT http://localhost:8080/api/v1/movie

### Get Character For Movie
GET /api/v1/movie/{{id}}/characters

### Updating Characher in Movie
PUT http://localhost:8080/api/v1/movie/{{movie_id}}/character/{{character_id}}

### Updating Movie In Faranchise
PUT http://localhost:8080/api/v1/movie/{{movie_id}}/franchise/{{franchise_id}}

#Authors AtmarKohistany(@atmaratmar) and @RameshPagilla
      
