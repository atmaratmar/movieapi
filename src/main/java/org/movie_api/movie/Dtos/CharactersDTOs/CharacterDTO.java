package org.movie_api.movie.Dtos.CharactersDTOs;

import lombok.Data;
import org.movie_api.movie.models.Movie;

import java.util.Set;

@Data
public class CharacterDTO {
    private Long character_Id;
    private String character_fullName;
    private String character_alias;
    private Set<Movie> movisDTOS;

}
