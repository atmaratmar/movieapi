package org.movie_api.movie.Dtos.MoviesDTOs;

import lombok.Data;
import org.movie_api.movie.models.Movie;

import java.util.Set;
@Data
public class MoviesDTO {
    private Long movie_id;
    private String movie_title;
    private String movie_genre;
    private int movie_year;
    private String movie_director;
    private String movie_picture;
    private String movie_trailer;
}
