package org.movie_api.movie;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.function.Predicate;

@SpringBootApplication
@EnableSwagger2
public class MovieApplication {
	@Bean
	public ModelMapper modelMapper(){
		return new ModelMapper();
	}
	public static void main(String[] args) {SpringApplication.run(MovieApplication.class, args);}

	@Bean
	public Docket postsApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo()).select()
				.apis(RequestHandlerSelectors.basePackage("org.movie_api.movie"))
				.build();
	}
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("simplifyingtech API")
				.description("simplifyingtech API for developers")
				.termsOfServiceUrl("http://localhost:8080/api/v1/franchise/{{id}}/characters/")
				.licenseUrl("a@gmail.com").version("2.0").build();
	}

}

