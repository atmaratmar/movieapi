package org.movie_api.movie.Services;

import org.modelmapper.ModelMapper;
import org.movie_api.movie.Dtos.CharactersDTOs.CharacterDTO;
import org.movie_api.movie.models.Character;
import org.movie_api.movie.repositories.CharactersRepository.CharacterRepository;
import org.movie_api.movie.repositories.MoviesRepository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@Service
public class CharacterService {
    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private MovieRepository movieRepository;

    public List<CharacterDTO> getAllCharactersDTO
            () {
        return characterRepository.findAll()
                .stream()
                .map(this::convertEntityToDto)
                .collect(Collectors.toList());
    }

    private CharacterDTO convertEntityToDto(Character character) {
        CharacterDTO characterDTO = new CharacterDTO();
        characterDTO.setCharacter_Id(character.getCharacter_id());
        characterDTO.setCharacter_fullName(character.getCharacter_fullName());
        characterDTO.setCharacter_alias(character.getCharacter_alias());
        return characterDTO;
    }

}
