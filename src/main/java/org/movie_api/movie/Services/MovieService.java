package org.movie_api.movie.Services;

import org.modelmapper.ModelMapper;
import org.movie_api.movie.Dtos.MoviesDTOs.MoviesDTO;
import org.movie_api.movie.models.Movie;
import org.movie_api.movie.repositories.MoviesRepository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieService {
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ModelMapper modelMapper;

    public List<MoviesDTO> FindAllMovies() {
        return movieRepository.findAll()
                .stream()
                .map(this::convertEntity)
                .collect(Collectors.toList());
    }

    private MoviesDTO convertEntity(Movie movie) {
        return getMoviesDTO(movie);
    }

    private MoviesDTO getMoviesDTO(Movie movie) {
        MoviesDTO moviesDTO = new MoviesDTO();
        moviesDTO.setMovie_id(movie.getMovie_id());
        moviesDTO.setMovie_title(movie.getMovie_title());
        moviesDTO.setMovie_director(movie.getMovie_director());
        moviesDTO.setMovie_genre(movie.getMovie_genre());
        moviesDTO.setMovie_picture(movie.getMovie_picture());
        moviesDTO.setMovie_trailer(movie.getMovie_trailer());
        moviesDTO.setMovie_year(movie.getMovie_year());
        return moviesDTO;
    }

    public List<MoviesDTO> FindById(Long id) {
        return movieRepository.findById(id)
                .stream()
                .map(this::convertEntityId)
                .collect(Collectors.toList());
    }
    private MoviesDTO convertEntityId(Movie movie) {
        return getMoviesDTO(movie);
    }


}
