package org.movie_api.movie.controller;
import org.movie_api.movie.models.Character;
import org.movie_api.movie.repositories.CharactersRepository.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/character")
public class CharacterController {
    @Autowired
    CharacterRepository characterRepository;
    /**
     * ### Get All Characters
     * GET https://localhost:8080/api/v1/characters
     * @return
     */
    @GetMapping
    public ResponseEntity<List<Character>> getAllCharacter() {
        List<Character> characters = characterRepository.findAll();
        HttpStatus statusCode = HttpStatus.OK;
        return new ResponseEntity<>(characters, statusCode);
    }
    /**
     * ### Get Character By Id
     * GET http://localhost:8080/api/v1/character/{{id}}
     * @param id
     * @return
     */
    @GetMapping(value = "/{id}")
    public ResponseEntity<Character> getCharacterById(@PathVariable Long id) {
        Character character = characterRepository.getById(id);
        HttpStatus statusCode = HttpStatus.NOT_FOUND;
        //Check that the character exists
        if (character == null) {
            return new ResponseEntity<>(character, statusCode);
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(characterRepository.getById(id));
        }
    }
    /**
     *
     ### Add Character
     POST http://localhost:8080/api/v1/character
     * @param character
     * @return
     */
    @PostMapping
    public ResponseEntity<Character> addNewCharacter(@RequestBody Character character) {
        if (character.getCharacter_fullName() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(characterRepository.save(character), HttpStatus.CREATED);
    }
    /**
     * ### Update Character
     * PUT http://localhost:8080/api/v1/character
     * @param newCharacter
     * @return
     */
    @PutMapping
    public ResponseEntity<?> updateCharacter(@RequestBody Character newCharacter) {
        if (newCharacter.getCharacter_fullName() == null) {//Checking that it has a name as that field is required
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("The character need to have a name");
        } else {
            characterRepository.getById(newCharacter.getCharacter_id());//Checkign that the character exists
            Character updatedCharacter = characterRepository.save(newCharacter);
            return ResponseEntity.status(HttpStatus.OK).body(updatedCharacter);
        }
    }
}
