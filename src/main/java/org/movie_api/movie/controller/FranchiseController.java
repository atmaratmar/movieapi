package org.movie_api.movie.controller;

import org.movie_api.movie.models.Character;
import org.movie_api.movie.models.Franchise;
import org.movie_api.movie.models.Movie;
import org.movie_api.movie.repositories.FranchiseRepository.FranchiseRepository;
import org.movie_api.movie.repositories.MoviesRepository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/franchise")
public class FranchiseController {
    @Autowired
    private FranchiseRepository franchiseRepository;
    /**
     * ### Get All Franchise
     * GET http://localhost:8080/api/v1/franchise
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        List<Franchise> franchises = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchises, status);
    }

    /**
     * ### Get Franchise By Id
     * GET http://localhost:8080/api/v1/franchise/{{id}}
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id) {
        Franchise franchise = new Franchise();
        HttpStatus status;
        if (franchiseRepository.existsById(id)) {//Check that the franchise actually exists
            status = HttpStatus.OK;
            franchise = franchiseRepository.findById(id).get();
        } else {
            System.out.println("Franchise not found");
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(franchise, status);
    }

    /**
     * ### Add New Franchise
     * POST http://localhost:8080/api/v1/franchise
     * @param franchise
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) {
        franchise = franchiseRepository.save(franchise);
        HttpStatus resp = HttpStatus.CREATED;
        System.out.println("Created new franchise with id " + franchise.getFranchise_id());
        return new ResponseEntity<>(franchise, resp);
    }

    /**
     * ### Update Franchise
     * PUT http://localhost:8080/api/v1/franchise
     * @param franchise
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> updateFranchise(@RequestBody Franchise franchise) {
        HttpStatus status;
        Franchise found = franchiseRepository.getById(franchise.getFranchise_id());
        if (found == null) {//Checking if the franchise actually exists
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>("franchise with id " + franchise.getFranchise_id() + " does not exists", status);
        }
        Franchise returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.OK;
        return new ResponseEntity<>(returnFranchise, status);
    }

    /**
     * ### Delete Franchise
     * DELETE http://localhost:8080/api/v1/franchise/{{id}}
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteFranchise(@PathVariable Long id) {
        HttpStatus status;
        String message = "";
        if (franchiseRepository.existsById(id)) {//Checking if franchise exists
            franchiseRepository.deleteById(id);
            System.out.println("Deleted franchise with id " + id);
            message = "Deleted franchise with id " + id;
            status = HttpStatus.OK;
        } else {
            System.out.println("Franchise with id " + id + " not found");
            message = "Franchise with id " + id + " not found";
            status = HttpStatus.NOT_FOUND;
        }
        return ResponseEntity.status(status).body(message);
    }

    /**
     * ### Get Movie by Franchise Id
     * GET http://localhost:8080/api/v1/franchise/{{id}}/movies
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}/movies", method = RequestMethod.GET)
    public ResponseEntity<?> getMoviesForAFranchise(@PathVariable Long id) {
        Franchise franchise = new Franchise();
        HttpStatus status;
        if (franchiseRepository.existsById(id)) {//Check that the franchise actually exists
            System.out.println("Found franchise with id " + id);
            status = HttpStatus.OK;
            franchise = franchiseRepository.findById(id).get();
        } else {
            System.out.println("Franchise not found");
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(franchise.getMovies(), status);
    }

    /**
     * ### Get Franchise,s Character
     * GET http://localhost:8080/api/v1/franchise/{{id}}/characters
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}/characters", method = RequestMethod.GET)
    public ResponseEntity<?> getCharactersForAFranchise(@PathVariable Long id) {
        Franchise franchise = franchiseRepository.findById(id).get();
        if (franchise == null) {//Checking if the franchise exists
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Id does not exist");
        } else {
            //Getting the movies related to the franchise
            List<Movie> franchiseMovies = franchise.getMovies();
            List<Character> charList = new ArrayList<>();
            for (int i = 0; i < franchiseMovies.size(); i++) {
                //Getting the characters from every movie thats related to the franchise
                charList.addAll(franchiseMovies.get(i).getCharacterSet());
                // charList.addAll(movieList.get(1).getCharacterSet());
            }
            //Hacky way to remove duplicated from the list
            Set<Character> set = new HashSet<>(charList);
            charList.clear();
            charList.addAll(set);
            return ResponseEntity.status(HttpStatus.OK).body(charList);
        }
    }

}

