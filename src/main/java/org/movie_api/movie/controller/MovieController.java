package org.movie_api.movie.controller;
import org.movie_api.movie.Dtos.MoviesDTOs.MoviesDTO;
import org.movie_api.movie.Services.MovieService;
import org.movie_api.movie.models.Character;
import org.movie_api.movie.models.Franchise;
import org.movie_api.movie.models.Movie;
import org.movie_api.movie.repositories.CharactersRepository.CharacterRepository;
import org.movie_api.movie.repositories.FranchiseRepository.FranchiseRepository;
import org.movie_api.movie.repositories.MoviesRepository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/movie")
public class MovieController {
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private CharacterRepository characterRepository;
    @Autowired
    private MovieService movieService;
    @Autowired
    private FranchiseRepository franchiseRepository;
    /**
     *### Get All Movies
     * GET http://localhost:8080/api/v1/movies
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAllMovies() {
        return ResponseEntity.status(HttpStatus.OK).body(movieService.FindAllMovies());
    }
    /**
     *### Get Movies By Id
     * GET http://localhost:8080/api/v1/movies/{{id}}
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getMovie(@PathVariable Long id) {
        List<MoviesDTO> movie = movieService.FindById(id);
        if (movie != null) {
            return ResponseEntity.status(HttpStatus.OK).body(movie);
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("movie with id " + id + " does not exists");
    }
    /**
     *### Delete Move
     * POST http://localhost:8080/api/v1/movies
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteMovie(@PathVariable Long id) {
        Movie found = movieRepository.getById(id);
        if (found == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Movie with id " + id + " does not exist");
        }
        movieRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).body("Movie with id " + id + " was deleted");
    }
    /**
     *
     ### Add new Movie
     PUT http://localhost:8080/api/v1/movies
     * @param movie
     * @return
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Movie> addNewMovie(@RequestBody Movie movie) {
        if (movie.getMovie_title() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(movieRepository.save(movie), HttpStatus.CREATED);
    }
    /**
     ### Update Movie
     PUT http://localhost:8080/api/v1/movies
     * @param movie
     * @return
     */
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<?> updateMovie(@RequestBody Movie movie) {
        if (movie.getMovie_id() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing id");
        } else if (movie.getMovie_title() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing title");
        } else if (movie.getMovie_genre() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing genre");
        } else if (movie.getMovie_year() == 0) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing year");
        } else if (movie.getMovie_director() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing director");
        } else if (movie.getMovie_picture() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing picture");
        } else if (movie.getMovie_trailer() == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Missing trailer");
        }
        Movie found = movieRepository.getById(movie.getMovie_id());
        if (found == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Movie with " + movie.getMovie_id() + " id, does not exist");
        }
        Movie updatedMovie = movieRepository.save(movie);
        return ResponseEntity.status(HttpStatus.OK).body(updatedMovie);
    }

    /**
     * ### Get Character For Movie
     * GET /api/v1/movies/{{id}}/characters
     * @param id
     * @return
     */
    @RequestMapping(value = "/{id}/characters")
    public ResponseEntity<?> getCharactersForAMovie(@PathVariable Long id) {
        Movie movie;
        movie = movieRepository.getById(id);
        if (movie != null) {
            return ResponseEntity.status(HttpStatus.OK).body(movie.getCharacterSet());
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("movie with id " + id + " does not exists");
        }
    }
    /**
     * ### Updating Characher in Movie
     * PUT http://localhost:8080/api/v1/movie/{{movie_id}}/character/{{character_id}}
     * @param
     * @return
     */
    @RequestMapping(value = "/{movie_id}/character/{character_id}", method = RequestMethod.PUT)
    Movie addcharacterToMovie(
            @PathVariable Long movie_id,
            @PathVariable Long character_id
    ) {
        Movie movie = movieRepository.findById(movie_id).get();
        Character character = characterRepository.findById(character_id).get();
        movie.addchararcterToMovie(character);
        return movieRepository.save(movie);
    }

    /**
     * ### Updating Movie In Faranchise
     * PUT http://localhost:8080/api/v1/movie/{{movie_id}}/franchise/{{franchise_id}}
     * @param movie_id
     * @param franchise_id
     * @return
     */
    @RequestMapping(value = "/{movie_id}/franchise/{franchise_id}", method = RequestMethod.PUT)
    Movie addMovieFranchise(
            @PathVariable Long movie_id,
            @PathVariable Long franchise_id
    ) {
        Movie movie = movieRepository.findById(movie_id).get();
        Franchise franchise = franchiseRepository.findById(franchise_id).get();
        movie.addMovieToFranchise(franchise);
        return movieRepository.save(movie);
    }
}
