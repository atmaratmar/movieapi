package org.movie_api.movie.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "character_id")
public class Character {
    @ManyToMany(mappedBy = "characterSet", cascade = CascadeType.ALL)
    public Set<Movie> movies = new HashSet<Movie>();
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long character_id;
    private String character_fullName;
    private String character_alias;
    private String character_gender;
    private String character_picture;

    @JsonGetter("movies")
    public List<Object> movies() {
        if (movies != null) {
            return movies.stream()
                    .map(movie -> {
                        return movie.getMovie_id();

                    }).collect(Collectors.toList());
        }
        return null;
    }

    public long getCharacter_id() {
        return character_id;
    }

    public void setCharacter_id(long character_id) {
        this.character_id = character_id;
    }

    public String getCharacter_fullName() {
        return character_fullName;
    }

    public void setCharacter_fullName(String character_fullName) {
        this.character_fullName = character_fullName;
    }

    public String getCharacter_alias() {
        return character_alias;
    }

    public void setCharacter_alias(String character_alias) {
        this.character_alias = character_alias;
    }

    public String getCharacter_gender() {
        return character_gender;
    }

    public void setCharacter_gender(String character_gender) {
        this.character_gender = character_gender;
    }

    public String getCharacter_picture() {
        return character_picture;
    }

    public void setCharacter_picture(String character_picture) {
        this.character_picture = character_picture;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

}
