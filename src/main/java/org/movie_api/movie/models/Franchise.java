package org.movie_api.movie.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long franchise_id;
    private String franchise_name;
    private String franchise_description;
    @JsonIgnore
    @OneToMany(mappedBy = "franchise")
    private List<Movie> movies;

    public Long getFranchise_id() {
        return franchise_id;
    }

    public void setFranchise_id(Long franchise_id) {
        this.franchise_id = franchise_id;
    }

    public String getFranchise_name() {
        return franchise_name;
    }

    public void setFranchise_name(String franchise_name) {
        this.franchise_name = franchise_name;
    }

    public String getFranchise_description() {
        return franchise_description;
    }

    public void setFranchise_description(String franchise_description) {
        this.franchise_description = franchise_description;
    }
    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public void addMovieToFranchise(Movie movie) {
        movies.add(movie);
    }
}
