package org.movie_api.movie.repositories.CharactersRepository;

import org.movie_api.movie.models.Character;
import org.movie_api.movie.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CharacterRepository extends JpaRepository<Character,Long> {
}
