package org.movie_api.movie.repositories.FranchiseRepository;

import org.movie_api.movie.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FranchiseRepository extends JpaRepository<Franchise,Long> {
}
