package org.movie_api.movie.repositories.MoviesRepository;

import org.movie_api.movie.Dtos.MoviesDTOs.MoviesDTO;
import org.movie_api.movie.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<Movie,Long> {
}

